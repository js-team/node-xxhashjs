# Installation
> `npm install --save @types/xxhashjs`

# Summary
This package contains type definitions for xxhashjs (https://github.com/pierrec/js-xxhash).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/xxhashjs.

### Additional Details
 * Last updated: Fri, 15 May 2020 13:13:07 GMT
 * Dependencies: [@types/node](https://npmjs.com/package/@types/node)
 * Global values: `XXH`

# Credits
These definitions were written by [Dibyo Majumdar](https://github.com/mDibyo), and [Nick Zahn](https://github.com/Manc).
